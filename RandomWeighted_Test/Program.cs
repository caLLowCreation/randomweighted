﻿using RandomWeighted;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RandomWeighted_Test
{
    public class Book : IWeighted
    {
        public int Isbn { get; set; }
        public string Name { get; set; }
        public int Weight { get; set; }
    }
    
    class Program
    {
        static void Main(string[] args)
        {
            var books = new List<Book> {
                new Book{Isbn=1,Name="A",Weight=1},
                new Book{Isbn=2,Name="B",Weight=100},
                new Book{Isbn=3,Name="C",Weight=1000},
                new Book{Isbn=4,Name="D",Weight=10000},
                new Book{Isbn=5,Name="E",Weight=100000}
            };

            Dictionary<string, int> weights = new Dictionary<string, int>();
            weights.Add("A", 0);
            weights.Add("B", 0);
            weights.Add("C", 0);
            weights.Add("D", 0);
            weights.Add("E", 0);
            /*for (int i = 0; i < 1000000; i++)
            {
                Book randomlySelectedBook = WeightedRandomization.Choose(books);
                weights[randomlySelectedBook.Name] += 1;
            }*/
            Parallel.For(0, 1000000, (int i) => {
                Book randomlySelectedBook = WeightedRandomization.Choose(books);
                weights[randomlySelectedBook.Name] += 1;
            });


            Console.WriteLine("Count {0}", weights.Count);
        }
    }
}
