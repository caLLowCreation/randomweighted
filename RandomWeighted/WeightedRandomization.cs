﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RandomWeighted
{
    public interface IWeighted
    {
        int Weight { get; set; }
    }

    public class WeightedRandomization
    {
        public static T Choose<T>(List<T> list) where T : IWeighted
        {
            if (list.Count == 0)
            {
                return default(T);
            }

            int totalweight = list.Sum(c => c.Weight);
            Random rand = new Random();
            int choice = rand.Next(totalweight);
            int sum = 0;

            foreach (var obj in list)
            {
                for (int i = sum; i < obj.Weight + sum; i++)
                {
                    if (i >= choice)
                    {
                        return obj;
                    }
                }
                sum += obj.Weight;
            }

            return list.First();
        }


    }
}
